FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/customer-service/customer-service.jar"]

ADD target/customer-service.jar /usr/share/customer-service/customer-service.jar